-------------------------------------------------------------------------------

                   Projet pydynamics sous Mulcyber (2013)

Commande pour acceder de maniere anonyme au depot SVN du projet pydynamics
dans sa totalite (trunk, branches, tags) :
svn export svn://scm.mulcyber.toulouse.inra.fr/svnroot/pydynamics

-------------------------------------------------------------------------------

Le projet pydynamics a ete ouvert dans le cadre du stage d'Anne-Laure Sabadel.

Il a servi au developpement des paquets pydynamics et openalea qui, une fois 
prets, ont ete livres au projet recordb.

Il sert d'archive des travaux effectues par Anne-Laure Sabadel dans le cadre 
de son stage (mai-juillet 2013).

Les paquets produits pydynamics et openalea se trouvent maintenant dans le
projet recordb https://mulcyber.toulouse.inra.fr/projects/recordb (dans son
depot git, sous le repertoire pkgs).

-------------------------------------------------------------------------------
Contenu du depot svn du projet pydynamics :

*** tags/pydynamics_20130829.tar.gz,openalea_20130829.tar.gz :
    archive des paquets pydynamics et openalea tels que livres au projet
    recordb sur son depot git.

    pydynamics, a package to program vle Dynamics models in python language.
    openalea, a package to invoke models from the openalea platform.

*** trunk : vide depuis livraison du code developpe au projet recordb.

*** branches/010_travaux_alsabadel : archive des travaux effectues par 
    Anne-Laure Sabadel dans le cadre de son stage (mai-juillet 2013).
    Code et traces resultats concernant :
      - les travaux a Montpellier.
      - le couplage entre le modele openalea AdelCaribu et le modele vle 2CV.

*** autres branches : diverses sauvegardes en cours de developpement 

-------------------------------------------------------------------------------

