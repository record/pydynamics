/* Module Dynamics */
%module(directors="1") PyDynamics

%ignore operator<<;

%{
#include <vle/DllDefines.hpp>
#include <vle/devs/Time.hpp>

#include <vle/value/Value.hpp>
#include <vle/value/Double.hpp>
#include <vle/value/Integer.hpp>
#include <vle/value/Boolean.hpp>
#include <vle/value/String.hpp>
#include <vle/value/Null.hpp>
/* #include <vle/value/Table.hpp> (boost) */
#include <vle/value/Tuple.hpp>
/* #include <vle/value/Matrix.hpp> (boost) */
#include <vle/value/XML.hpp>
#include <vle/value/Map.hpp>
#include <vle/value/Set.hpp>

#include <vle/devs/InitEventList.hpp>
#include <vle/devs/ExternalEvent.hpp>
#include <vle/devs/ExternalEventList.hpp>
#include <vle/devs/ObservationEvent.hpp>
#include <vle/vpz/AtomicModel.hpp>

#include <vle/utils/PackageTable.hpp>
#include <vle/version.hpp>
#include <vle/devs/Dynamics.hpp>
#include <Python.h>

/* +++ Extensions */ /* !!!!  ... essais en cours ... */
//     
//     #include <vle/extension/celldevs/CellDevs.hpp>
//     
//     #include <vle/extension/cellqss/CellQSS.hpp>
//     
//     #include <vle/extension/decision/Activities.hpp>
//     #include <vle/extension/decision/Activity.hpp>
//     #include <vle/extension/decision/Agent.hpp>
//     #include <vle/extension/decision/Facts.hpp>
//     #include <vle/extension/decision/KnowledgeBase.hpp>
//     #include <vle/extension/decision/Library.hpp>
//     #include <vle/extension/decision/Plan.hpp>
//     #include <vle/extension/decision/PrecedenceConstraint.hpp>
//     #include <vle/extension/decision/PrecedencesGraph.hpp>
//     #include <vle/extension/decision/Predicates.hpp>
//     #include <vle/extension/decision/Rule.hpp>
//     #include <vle/extension/decision/Rules.hpp>
//     #include <vle/extension/decision/Table.hpp>
//     #include <vle/extension/decision/Version.hpp>
//     
//     #include <vle/extension/difference-equation/Base.hpp>
//     #include <vle/extension/difference-equation/GenericDbg.hpp>
//     #include <vle/extension/difference-equation/Generic.hpp>
//     #include <vle/extension/difference-equation/MultipleDbg.hpp>
//     #include <vle/extension/difference-equation/Multiple.hpp>
//     #include <vle/extension/difference-equation/SimpleDbg.hpp>
//     #include <vle/extension/difference-equation/Simple.hpp>
//     
//     /* #include <vle/extension/differential-equation/DifferentialEquation.hpp> */
//     
//     /* #include <vle/extension/fsa/Moore.hpp> */
//     /* #include <vle/extension/fsa/Mealy.hpp> */
//     /* #include <vle/extension/fsa/FDDevs.hpp> */
//     #include <vle/extension/fsa/Statechart.hpp>
//     
//     /* #include <vle/extension/petrinet/PetriNet.hpp> */
//     

%}


%{
using namespace vle;
using namespace vle::devs;
using namespace vle::value;
using namespace vle::vpz;
using namespace vle::utils;
using namespace std;
%}

%include <std_string.i>
%include <std_vector.i>
%include <std_map.i>
%include <std_set.i>
%include <stdint.i> 
%include <stl.i>


%template(ExternalEventList) std::vector < vle::devs::ExternalEvent* >;

/* %template(TableValue) boost::multi_array < double, 2 >; */
%template(TupleValue) std::vector < double >;
/* %template(MatrixValue) boost::multi_array <vle::value::Value*, 2 >; */
%template(VectorValue) std::vector < vle::value::Value* >;
%template(MapValue) std::map < std::string, vle::value::Value* >;


%include <vle/DllDefines.hpp>

%include <vle/devs/Dynamics.hpp>
%include <vle/DllDefines.hpp>

%include <vle/value/Value.hpp>
%include <vle/value/Double.hpp>
%include <vle/value/Integer.hpp>
%include <vle/value/Boolean.hpp>
%include <vle/value/String.hpp>
%include <vle/value/Null.hpp>
/* %include <vle/value/Table.hpp> (boost) */
%include <vle/value/Tuple.hpp>
/*%include <vle/value/Matrix.hpp> (boost) */
%include <vle/value/XML.hpp>
%include <vle/value/Map.hpp>
%include <vle/value/Set.hpp>

%include <vle/devs/Time.hpp>
%include <vle/devs/InitEventList.hpp>
%include <vle/devs/ExternalEvent.hpp>
%include <vle/devs/ExternalEventList.hpp>

%include <vle/devs/ObservationEvent.hpp>
%include <vle/vpz/AtomicModel.hpp>
%include <vle/utils/PackageTable.hpp>
%include <vle/version.hpp>


/* typedef vle::value::Map InitEventList; */
/*     %pythoncode %{ */
/*    class InitEventList(Map) : pass */
/*     %} */

/* +++ Extensions */
//     
//     %include <vle/extension/celldevs/CellDevs.hpp>
//     
//     %include <vle/extension/cellqss/CellQSS.hpp>
//     
//     /* %include <vle/extension/decision/Activities.hpp> */
//     /* %include <vle/extension/decision/Activity.hpp> */
//     %include <vle/extension/decision/Agent.hpp>
//     /* %include <vle/extension/decision/Facts.hpp> */
//     /* %include <vle/extension/decision/KnowledgeBase.hpp> */
//     /* %include <vle/extension/decision/Library.hpp> */
//     /* %include <vle/extension/decision/Plan.hpp> */
//     /* %include <vle/extension/decision/PrecedenceConstraint.hpp> */
//     /* %include <vle/extension/decision/PrecedencesGraph.hpp> */
//     /* %include <vle/extension/decision/Predicates.hpp> notok */
//     /* %include <vle/extension/decision/Rule.hpp> */
//     /* %include <vle/extension/decision/Rules.hpp> */
//     /* %include <vle/extension/decision/Table.hpp> */
//     /* %include <vle/extension/decision/Version.hpp> */
//     
//     /* no <vle/extension/difference-equation/Base.hpp> (class Base) */
//     /*%include <vle/extension/difference-equation/GenericDbg.hpp>*/
//     /*%include <vle/extension/difference-equation/Generic.hpp>*/
//     %include <vle/extension/difference-equation/MultipleDbg.hpp>
//     %include <vle/extension/difference-equation/Multiple.hpp>
//     %include <vle/extension/difference-equation/SimpleDbg.hpp>
//     %include <vle/extension/difference-equation/Simple.hpp>
//     
//     /* %include <vle/extension/differential-equation/DifferentialEquation.hpp> notok ? */
//     
//     /* no <vle/extension/fsa/FSA.hpp> (class Base) */
//     /* %include <vle/extension/fsa/Moore.hpp> notok */
//     /* %include <vle/extension/fsa/Mealy.hpp> notok */
//     /* %include <vle/extension/fsa/FDDevs.hpp> notok */
//     %include <vle/extension/fsa/Statechart.hpp>
//     
//     /* %include <vle/extension/petrinet/PetriNet.hpp> notok */
//     

