/* Module Dynamics */
%module(directors="1") testDynamics
%{
 /**** includes ****/ 
#include <vle/DllDefines.hpp>
#include <vle/devs/Time.hpp>
#include <vle/devs/InitEventList.hpp>
#include <vle/devs/ExternalEvent.hpp>
#include <vle/devs/ExternalEventList.hpp>
#include <vle/devs/ObservationEvent.hpp>
#include <vle/vpz/AtomicModel.hpp>
#include <vle/value/Value.hpp>
#include <vle/value/Double.hpp>
#include <vle/value/Integer.hpp>
#include <vle/value/Boolean.hpp>
#include <vle/value/String.hpp>
#include <vle/utils/PackageTable.hpp>
#include <vle/version.hpp>
//#include <string>

//#include <vle/devs/Dynamics.hpp>
#include <vle/utils/Path.hpp>
#include <vle/utils/Package.hpp>
#include "testDynamics.hpp"

namespace vle { namespace devs {
	//class RootCoordinator;*
	//typedef utils::PackageTable::index PackageId;
	extern int b(int i);

}}
%}

%{
using namespace vle;
using namespace vle::devs;
using namespace vle::value;
using namespace vle::vpz;
using namespace vle::utils;
%}

//%include "std_string.i"
//%include "std_map.i"

// Test de function ********************************

namespace vle { namespace devs {
//	class RootCoordinator;
//	typedef utils::PackageTable::index PackageId;
	extern int b(int i);
}}
//**************************************************

//%feature("director") vle::value::Value::std::ostream& operator<<;
//%feature("director") vle::devs::ExternalEvent;
//***** rename *****
//%rename(lower) vle::value::ExternalEvent::operator<<;
//rename(lower) vle::value::ExternalEvent::operator<<
//**********************************

/**** includes ****/ 
#define DEVS_DYNAMICS_HPP
#define VLE_API
#define DECLARE_DYNAMICS(mdl)                                           \
    extern "C" {                                                        \
        VLE_MODULE vle::devs::Dynamics*                                 \
        vle_make_new_dynamics(const vle::devs::DynamicsInit& init,      \
                              const vle::devs::InitEventList& events)   \
        {                                                               \
            return new mdl(init, events);                               \
        }                                                               \
                                                                        \
        VLE_MODULE void                                                 \
        vle_api_level(vle::uint32_t* major,                             \
                      vle::uint32_t* minor,                             \
                      vle::uint32_t* patch)                             \
        {                                                               \
            *major = VLE_MAJOR_VERSION;                                 \
            *minor = VLE_MINOR_VERSION;                                 \
            *patch = VLE_PATCH_VERSION;                                 \
        }                                                               \
    }

//%feature("director") vle::value::Value;
//%feature("director") vle::value::Integer;

%include "testDynamics.hpp"
%include <vle/DllDefines.hpp>
%include <vle/devs/Time.hpp>
%include <vle/devs/InitEventList.hpp>
%include <vle/devs/ExternalEvent.hpp>
%include <vle/devs/ExternalEventList.hpp>
%include <vle/devs/ObservationEvent.hpp>
%include <vle/vpz/AtomicModel.hpp>
%include <vle/value/Value.hpp>
%include <vle/value/Double.hpp>
%include <vle/value/Integer.hpp>
%include <vle/value/Boolean.hpp>
%include <vle/value/String.hpp>
%include <vle/utils/PackageTable.hpp>
%include <vle/version.hpp>
//%include <string>

//%include <vle/devs/Dynamics.hpp>
%include <vle/utils/Path.hpp>
%include <vle/utils/Package.hpp>
