import pyDynamics

class plantlouse(pyDynamics.qss):
    def __init__(self, model, events):
        pyDynamics.qss.__init__(self, model, events)
        self.a = pyDynamics.toDouble(events.get("a"))
        self.b = pyDynamics.toDouble(events.get("b"))

    def compute(self):
        return self.a * self.getValue() - self.b * self.getValue("y") * self.getValue()
  
