import pyDynamics

class Pop(pyDynamics.CombinedQss):
    def __init__(self, model, events):
        pyDynamics.CombinedQss.__init__(self, model, events)
        self.a = pyDynamics.toDouble(events.get("a"))
        self.b = pyDynamics.toDouble(events.get("b"))
        self.d = pyDynamics.toDouble(events.get("d"))
	self.e = pyDynamics.toDouble(events.get("e"))

    def compute(self, i):
        if i == 0:
            return self.a * self.getValue(0) - self.b * self.getValue(0) * self.getValue(1);
        elif i == 1:
            return self.b * self.d * self.getValue(0) * self.getValue(1) - self.e * self.getValue(1)
