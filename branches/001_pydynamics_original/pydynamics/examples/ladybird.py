import pyDynamics

class ladybird(pyDynamics.qss):
    def __init__(self, model, events):
        pyDynamics.qss.__init__(self, model, events)
        self.b = pyDynamics.toDouble(events.get("b"))
        self.d = pyDynamics.toDouble(events.get("d"))
	self.e = pyDynamics.toDouble(events.get("e"))

    def compute(self):
        return self.b * self.d * self.getValue("x") * self.getValue() - self.e * self.getValue()
