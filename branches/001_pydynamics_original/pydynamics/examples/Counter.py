import pyDynamics

class Counter(pyDynamics.Dynamics):
    def __init__(self, model, events): 
        pyDynamics.Dynamics.__init__(self, model, events)
        self.counter = 0
        self.active = False
    
    def output(self, time, output):
	output.addEvent(self.buildEvent("out"))

    def timeAdvance(self):
        if self.active:
	    return pyDynamics.Time(1)
        else:
            return pyDynamics.Time.infinity

    def init(self, time): 
        counter = 0
        active = False
	return pyDynamics.Time.infinity
    
    def internalTransition(self, time):
        self.active = False;

    def externalTransition(self, eventlist, time):
        self.counter += 1
        self.active = True

    def observation(self, event):
        if event.onPort("c"):
            return self.buildDouble(self.counter)
        else:
            return None
