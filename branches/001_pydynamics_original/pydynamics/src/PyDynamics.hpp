/**
 * @file        PyDynamics.hpp
 * @author      The VLE Development Team.
 * @brief       Python wrapper
 */

/*
 * Copyright (c) 2004, 2005, 2006, 2007 The VLE Development Team.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#ifndef __PYDYNAMICS_HPP
#define __PYDYNAMICS_HPP

#include <Python.h> 
#include <vle/devs/DynamicsWrapper.hpp>
#include "swigpyrun.h"

class PyDynamics : public vle::devs::DynamicsWrapper {
private:
    // Swig types
    static swig_type_info *swig_InternalEvent;
    static swig_type_info *swig_ExternalEvent;
    static swig_type_info *swig_ExternalEventList;
    static swig_type_info *swig_ObservationEvent;
    static swig_type_info *swig_RequestEvent;
    static swig_type_info *swig_InitEventList;
    static swig_type_info *swig_Time;
    static swig_type_info *swig_Value;
    static swig_type_info *swig_AtomicModel;
  
    static unsigned int load;

    PyObject *pModule;
    bool init_module;
    const vle::devs::InitEventList& m_initEvents;

    //Private members
    void set_path();
    void load_types();
    void load_module(const std::string& library, 
		     const std::string& model,
		     const vle::graph::AtomicModel& model,
		     const vle::devs::InitEventList& events);
    void _test(PyObject *obj, const char *name) const;

    //Reference of Python object of DEVS model
    PyObject *pDict;
    PyTypeObject *pClass;
    PyObject *pSelf;

public:
    PyDynamics(const vle::graph::AtomicModel& /* model */,
	       const vle::devs::InitEventList& /* events */);
    virtual ~PyDynamics();

    virtual void output(const vle::devs::Time& /* time */,
			vle::devs::ExternalEventList& /* output */) const;
    virtual vle::devs::Time timeAdvance() const;
    virtual vle::devs::Time init(const vle::devs::Time& /* time */);
    virtual vle::devs::Event::EventType confluentTransitions(
	const vle::devs::Time& /* time */,
	const vle::devs::ExternalEventList& /* extEventlist */) const;
    virtual void internalTransition(const vle::devs::Time& /* time */);
    virtual void externalTransition(
	const vle::devs::ExternalEventList& /* event */,
	const vle::devs::Time& /* time */);
    virtual vle::value::Value observation(
	const vle::devs::ObservationEvent& /* event */) const;
    virtual void request(
	    const vle::devs::RequestEvent& /* event */,
            const vle::devs::Time& /* time */,
            vle::devs::ExternalEventList& /* output */) const;
};

DECLARE_DYNAMICS(PyDynamics);

#endif
