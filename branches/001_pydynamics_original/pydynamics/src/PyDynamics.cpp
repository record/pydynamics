/**
 * @file        PyDynamics.cpp
 * @author      The VLE Development Team.
 * @brief       Python wrapper
 */

/*
 * Copyright (c) 2004, 2005, 2006, 2007 The VLE Development Team.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "PyDynamics.hpp"
#include <vle/utils/Path.hpp>
#include <vle/utils/Trace.hpp>

using namespace vle::devs;
using namespace vle::graph;
using namespace vle::value;
using namespace std;

swig_type_info *PyDynamics::swig_InternalEvent = 0;
swig_type_info *PyDynamics::swig_ExternalEvent = 0;
swig_type_info *PyDynamics::swig_ExternalEventList = 0;
swig_type_info *PyDynamics::swig_ObservationEvent = 0;
swig_type_info *PyDynamics::swig_RequestEvent = 0;
swig_type_info *PyDynamics::swig_InitEventList = 0;
swig_type_info *PyDynamics::swig_Time = 0;
swig_type_info *PyDynamics::swig_Value = 0;
swig_type_info *PyDynamics::swig_AtomicModel = 0;

unsigned int PyDynamics::load = 0;

#define test(x) _test(x,#x)

void PyDynamics::_test(PyObject *obj, const char *name) const {
    if (!obj) {
	if (PyErr_Occurred()) PyErr_Print();
	throw vle::utils::InternalError(string("on ")+name);
    } 
}

PyDynamics::PyDynamics(const AtomicModel& model,
		       const InitEventList& events) : DynamicsWrapper(model, events),
						      m_initEvents(events)
{
    init_module = false;
    if (load == 0) {
	Py_Initialize();
	set_path();
    }
    load++;
}

PyDynamics::~PyDynamics() 
{ 
    Py_DECREF(pSelf);
    Py_DECREF(pModule);
    Py_DECREF(pDict);
    Py_DECREF(pClass);
    if (--load == 0) Py_Finalize();
}

void PyDynamics::set_path() 
{
    string total_path = string(Py_GetPath()) + ":.";

    PySys_SetPath((char *)total_path.c_str());
}

void PyDynamics::load_types() 
{
    swig_InternalEvent = SWIG_TypeQuery("vle::devs::InternalEvent *");
    swig_ExternalEvent = SWIG_TypeQuery("vle::devs::ExternalEvent *");
    swig_ExternalEventList = SWIG_TypeQuery("vle::devs::ExternalEventList *");
    swig_ObservationEvent = SWIG_TypeQuery("vle::devs::ObservationEvent *");
    swig_RequestEvent = SWIG_TypeQuery("vle::devs::RequestEvent *");
    swig_InitEventList = SWIG_TypeQuery("vle::devs::InitEventList *");
    swig_Time = SWIG_TypeQuery("vle::devs::Time *");
    swig_Value = SWIG_TypeQuery("vle::value::Value *");
    swig_AtomicModel = SWIG_TypeQuery("vle::graph::AtomicModel *");
}

void PyDynamics::load_module(const std::string& library,
			     const std::string& model,
			     const AtomicModel& atomic_model,
			     const InitEventList& events)
{
    PyObject *pName;
    PyObject *pTuple;

    TraceDebug((boost::format("PyDynamics: %1%:%2% ...") % library % model).str());

    pName = PyString_FromString(library.c_str());
    test(pName);

    pModule = PyImport_Import(pName);
    test(pModule);
    Py_DECREF(pName);

    load_types();
  
    pDict = PyModule_GetDict(pModule);
    test(pDict);
  
    pClass = (PyTypeObject*)PyDict_GetItemString(pDict, model.c_str());
  
    pTuple = PyTuple_New(2);
    test(pTuple);
  
    PyObject *pModel = SWIG_NewPointerObj((void*)(&atomic_model), 
					  swig_AtomicModel, 0);
    test(pModel);

    PyObject *pEvents = SWIG_NewPointerObj((void*)(&events), 
					   swig_InitEventList, 0);
    test(pEvents);

    PyTuple_SetItem(pTuple, 0, pModel);
    PyTuple_SetItem(pTuple, 1, pEvents);

    pSelf = PyType_GenericNew(pClass, pTuple, 0);
    test(pSelf);

    PyObject* pConstructor = PyObject_GetAttrString(pSelf, "__init__");
    test(pConstructor);

    PyObject *pResult = PyObject_Call(pConstructor, pTuple, 0);
    test(pResult);
 
    Py_DECREF(pTuple);

    init_module = true;
}

void PyDynamics::output(const Time& time,
			ExternalEventList& output) const
{
    PyObject *pTime = SWIG_NewPointerObj((void*)(&time), swig_Time, 0);
    test(pTime);

    PyObject *pEvent = SWIG_NewPointerObj((void*)(&output), 
					  swig_ExternalEventList, 0);
    test(pEvent);

    PyObject *pObject = PyObject_CallMethod(pSelf, (char*)"output", (char*)"OO", 
					    pTime, pEvent, 0);
    test(pObject);
}

Time PyDynamics::timeAdvance() const
{  
    PyObject *pTime = PyObject_CallMethod(pSelf, (char*)"timeAdvance", 0);
    test(pTime);

    Time* c_ptr = 0;
  
    SWIG_ConvertPtr(pTime, (void **) &c_ptr, swig_Time, 0);
    return Time(*c_ptr);
}

Time PyDynamics::init(const vle::devs::Time& time)
{
    if (not init_module)
	load_module(m_library, m_model, getModel(), m_initEvents);

    PyObject *pTime = SWIG_NewPointerObj((void*)(&time), swig_Time, 0);
    test(pTime);

    pTime = PyObject_CallMethod(pSelf, (char*)"init", (char*)"O", pTime, 0);
    test(pTime);
    
    Time* c_ptr = 0;

    SWIG_ConvertPtr(pTime, (void **) &c_ptr, swig_Time, 0);

    return Time(*c_ptr);
}

Event::EventType PyDynamics::confluentTransitions(const Time& time,
						  const ExternalEventList& extEventlist) const
{
    PyObject *pTime = SWIG_NewPointerObj((void*)(&time), swig_Time, 0);
    test(pTime);

    PyObject *pExternal = SWIG_NewPointerObj((void*)(&extEventlist), 
					     swig_ExternalEventList, 0);
    test(pExternal);

    PyObject *pObject = PyObject_CallMethod(pSelf, (char*)"confluentTransitions", (char*)"OO", 
					    pTime, pExternal, 0);
    test(pObject);

    return (Event::EventType)PyInt_AsLong(pObject);
}

void PyDynamics::internalTransition(const Time& time)
{
    PyObject *pTime = SWIG_NewPointerObj((void*)(&time), swig_Time, 0);
    test(pTime);

    PyObject *pObject = PyObject_CallMethod(pSelf, (char*)"internalTransition", 
					    (char*)"O", pTime, 0);
    test(pObject);
}

void PyDynamics::externalTransition(const ExternalEventList& event,
				    const Time& time)
{   
    PyObject *pEvent = SWIG_NewPointerObj((void*)(&event), 
					  swig_ExternalEventList, 0);
    test(pEvent);

    PyObject *pTime = SWIG_NewPointerObj((void*)(&time), swig_Time, 0);
    test(pTime);

    PyObject *pObject = PyObject_CallMethod(pSelf, (char*)"externalTransition", 
					    (char*)"OO", pEvent, pTime, 0);
    test(pObject);
}

Value PyDynamics::observation(const ObservationEvent& event) const
{
    PyObject *pEvent = SWIG_NewPointerObj((void*)(&event), swig_ObservationEvent, 0);
    test(pEvent);
    
    PyObject *pObject = PyObject_CallMethod(pSelf, (char*)"observation", (char*)"O", 
					    pEvent, 0);
    test(pObject);

    Value* c_ptr = 0;

    SWIG_ConvertPtr(pObject, (void **) &c_ptr, swig_Value, 0);
    return *c_ptr;
}

void PyDynamics::request(const RequestEvent& event,
			 const Time& time,
			 ExternalEventList& output) const
{
    PyObject *pEvent = SWIG_NewPointerObj((void*)(&event), 
					  swig_RequestEvent, 0);
    test(pEvent);

    PyObject *pTime = SWIG_NewPointerObj((void*)(&time), swig_Time, 0);
    test(pTime);

    PyObject *pOutput = SWIG_NewPointerObj((void*)(&output), 
					   swig_ExternalEventList, 0);
    test(pOutput);

    PyObject *pObject = PyObject_CallMethod(pSelf, (char*)"request", 
					    (char*)"OOO", pEvent, pTime, pOutput, 0);
    test(pObject);
}
