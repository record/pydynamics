%module(directors="1") pyDynamics
 
%{
#include <vle/devs/Dynamics.hpp>
#include <vle/devs/Event.hpp>
#include <vle/devs/ExternalEvent.hpp>
#include <vle/devs/RequestEvent.hpp>
#include <vle/devs/InternalEvent.hpp>
#include <vle/devs/ObservationEvent.hpp>
#include <vle/devs/Time.hpp>

#include <vle/value/Value.hpp>
#include <vle/value/Boolean.hpp>
#include <vle/value/Integer.hpp>
#include <vle/value/Double.hpp>

#include <vle/graph/AtomicModel.hpp>
#include <vle/vpz/Condition.hpp>

#include <vle/extension/CombinedQSS.hpp>
#include <vle/extension/QSS.hpp>
#include <vle/extension/DifferenceEquation.hpp>
%}

%{
using namespace vle;
using namespace vle::devs;
using namespace vle::graph;
using namespace vle::vpz;
using namespace vle::value;
using namespace vle::extension;
%}

%include "std_string.i"
%include "std_map.i"

%feature("director") vle::devs::Time;
%feature("director") vle::devs::Event;

%feature("director") vle::devs::ExternalEvent;

%include "vle/devs/EventList.hpp"

%feature("nodirector") ExternalEventList;
namespace vle { namespace devs {

class ExternalEventList
{   
    public:
	ExternalEventList();
	ExternalEventList(vle::devs::ExternalEvent* event);
        void addEvent(vle::devs::ExternalEvent* event);
	size_t size() const;
	vle::devs::ExternalEvent* at(size_t i) const;
	vle::devs::ExternalEvent* front();
	bool empty() const;
	void deleteAndClear();
};

} }

%include "vle/devs/ExternalEvent.hpp"

%feature("director") vle::value::Value;
%feature("director") vle::vpz::ValueList;

%feature("nodirector") vle::devs::InitEventList;
namespace vle { namespace devs {

class InitEventList
{
public:
   const vle::value::Value& get(const std::string& name) const;
   bool exist(const std::string& name) const;
};

} }


%feature("director") vle::value::Double;
%feature("director") vle::value::Integer;
%feature("director") vle::value::Boolean;

%include "vle/vpz/Condition.hpp"
%include "vle/devs/ExternalEventList.hpp"

%feature("director") vle::graph::AtomicModel;
%feature("director") vle::devs::Dynamics;

%feature("director") vle::devs::RequestEvent;
%feature("director") vle::devs::InternalEvent;
%feature("director") vle::devs::ObservationEvent;

// ***** Extensions *****

%feature("director") vle::extension::CombinedQss;
%feature("director") vle::extension::qss;
%feature("director") vle::extension::DifferenceEquation;

// ***** rename *****

%rename(add) vle::devs::Time::operator+;
%rename(auto_add) vle::devs::Time::operator+=;
%rename(inc) vle::devs::Time::operator++;
%rename(sub) vle::devs::Time::operator-;
%rename(auto_sub) vle::devs::Time::operator-=;
%rename(dec) vle::devs::Time::operator--;
%rename(equals) vle::devs::Time::operator==;
%rename(equals) vle::devs::Event::operator==;
%rename(not_equals) vle::devs::Time::operator!=;
%rename(greater_than) vle::devs::Time::operator>;
%rename(greater_than) vle::devs::Event::operator>;
%rename(lower_than) vle::devs::Time::operator<;
%rename(lower_than) vle::devs::Event::operator<;
%rename(greater_or_equal_than) vle::devs::Time::operator>=;
%rename(lower_or_equal_than) vle::devs::Time::operator<=;

// ***** includes *****

%include "vle/devs/Dynamics.hpp"
%include "vle/devs/Event.hpp"
%include "vle/devs/RequestEvent.hpp"
%include "vle/devs/InternalEvent.hpp"
%include "vle/devs/ObservationEvent.hpp"
%include "vle/devs/Time.hpp"

%include "vle/value/Value.hpp"
%include "vle/value/Boolean.hpp"
%include "vle/value/Integer.hpp"
%include "vle/value/Double.hpp"

%include "vle/graph/AtomicModel.hpp"

// ***** Extensions *****

%include "vle/extension/CombinedQSS.hpp"
%include "vle/extension/QSS.hpp"
%include "vle/extension/DifferenceEquation.hpp"

